var passport = require('passport')
var LocalStrategy = require('passport-local').Strategy;
var config = require('config');

var usersToPasswords = {};
config.get("listerific.users").forEach(function(userInfo) {
  usersToPasswords[userInfo.username] = userInfo.password;
});

passport.use(new LocalStrategy(
    function(username, password, done) {
      var storedPassword = usersToPasswords[username];
      if (typeof password === 'undefined' || password != storedPassword)
        return done(null, false, {message: "Invalid credentials"} );
      return done(null, username);
    }
));

function requireAuth(req, res, next) {
  if (typeof req.user !== 'undefined') {
    next();
  } else {
    res.redirect('/login');
  }
}

passport.serializeUser(function(username, done) {
  done(null, username);
});

passport.deserializeUser(function(username, done) {
  done(null, username);
});

module.exports = {
  requireAuth: requireAuth
};