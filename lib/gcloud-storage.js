var gcloud = require('gcloud');
var config = require('config');

var storage = gcloud.storage({
  projectId: config.get('listerific.gcloud.projectId'),
  keyFilename: __dirname + '/../config/gcloud-creds.json'
});

var bucket = storage.bucket(config.get('listerific.gcloud.bucket'));

function getBucketFile(filename) {
  return bucket.file(filename);
}

function getFileListing(callback) {
  bucket.getFiles(function(err, fileInfo) {
    if (err) {
      callback(err);
    } else {
      var fileItems = fileInfo.map(function(fi) {
        return {link: "/files/" + fi.name, metadata: fi.metadata};
      });
      callback(null, fileItems);
    }
  });
}

module.exports = {
  getFileListing: getFileListing,
  getBucketFile: getBucketFile
}