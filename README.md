# Listerific

(Let's say the name means **Lister** of **I**nternal **F**iles **i**n **C**loud storage).

A node.js webapp which simply provides a listing of files from a designated bucket in Google Cloud Storage and allows 
users, after they have authenticated, to download them.

Extremely bare bones right now (and that may be all that happens to it).

## Why does this exist?

Cloud-based files storage options, including Google Cloud Storage, are attractive for a variety of reasons, such as
a fairly low cost per gigabyte (particularly with Nearline storage). If you just want to privately share these files
without requiring people to access them through the Google Developer Console (and don't want any other bells
and whistles), this webapp may help.


## Installation

1. Make sure you have [node.js](http://nodejs.org/) installed
1. Download a tarball of Listerific or clone the repository, and extract it somewhere

## Configuration

You need to place two files in the `config` directory. The first will be loaded using the rules of 
[node-config](https://github.com/lorenwest/node-config), so the simplest option is to just call it `default.json`. 
It should look something like this, with the values replaced as appropriate.:

    {
      "listerific": {
        "gcloud": {
          "projectId": "googleprojectid",
          "bucket": "googlebucketid"
        },
        "users": [
          {
            "username": "andy",
            "password": "changeme"
          }
        ]
      }
    }
    
The `gcloud` items depend on what you have configured in the 
[Google Developer Console](https://console.developers.google.com/project) . The `users` key (which can of course
have multipled elements in the array) controls the allowed credentials. Yes, there should be more options here, or
at least the possibility to encrypt the passwords, but the current version suffices for my needs at the moment.

The second config file needed is a JSON file called `gcloud-creds.json` containing a set of Google Cloud client 
credentials for a service account with permissions for reading and listing files in the configured bucket. 
You can obtain this from the developer console under "Credentials" by selection "Generate New JSON key".

## Running

From the directory where you extracted the source code, run:

    $ npm install
    $ npm run bundle
    $ npm run start

(At this point you should probably set up some more robust option to keep Node running using Systemd or Upstart
or whatever).

## License

Apache 2.0