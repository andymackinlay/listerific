var express = require('express');
var router = express.Router();
var passport = require('passport');

var auth = require('../lib/auth.js');
var gcs = require('../lib/gcloud-storage.js');

/* GET home page. */
router.get('/', auth.requireAuth, function(req, res, next) {
  gcs.getFileListing(function(err, fileItems) {
    res.render('index', {fileItems: fileItems});
  });
});

router.get('/login', function(req, res, next) {
  res.render('login', {message: req.flash('error')});
});

router.post('/login',
    passport.authenticate('local', {
      successRedirect: '/',
      failureRedirect: '/login',
      failureFlash: true
    })
);

router.get('/logout', function(req, res) {
  req.logout();
  res.redirect('/');
});

router.get('/files/*', auth.requireAuth, function(req, res, next) {
  res.setHeader("Content-Type", "application/octet-stream");
  var file = gcs.getBucketFile(req.params[0]);
  file.getMetadata(function(err, metadata) {
    res.setHeader("Content-Length", metadata.size);
    file.createReadStream().pipe(res);
  });
});

module.exports = router;
